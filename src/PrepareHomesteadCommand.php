<?php namespace WorkshopOrange\Devspot;

use Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class PrepareHomesteadCommand extends DevspotCommandBase
{
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('prepare-homestead')
            ->setDescription('Prepare your Workshop Orange installation of Homestead')
            ->setHelp("This will change your Homestead.yaml file to support the command that come with the Devspot tools")
            ;

        $this->addOption('skip-homestead-site-setup','shss',
          InputOption::VALUE_NONE,
          'Instruct the script to not configure a homestead site'
        );

        $this->addOption('skip-homestead-db-setup','shds',
          InputOption::VALUE_NONE,
          'Instruct the script to not configure a homestead DB'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
          self::prepareConfig($input->getOption('config'), FALSE);

          if(!$this->config['HomesteadConfig']
           || !$this->config['paths']
           || !$this->config['paths']['project']
           || !$this->config['paths']['root']
           || !$this->config['paths']['code']) {
            throw new Exception('The following config keys are required: HomesteadConfig, paths:project, paths:root, paths:code');
          }
        } catch(Exception $ex) {
          $output->writeln('<error>Error processing the application config: ' . $ex->getMessage() . '</error>');
          return DevspotCommandBase::DS_CONFIG_FILE_ERROR;
        }
        
        $homesteadYamlPath =  Utils::processPath($this->config['HomesteadConfig']);
        $woCodeDir = Utils::processPath($this->config['paths']['code']);
        $rootCodeDir = Utils::processPath($this->config['paths']['root']);
        $projectDir = Utils::processPath($this->config['paths']['project']);
        $vmCodeRoot = Utils::processPath($this->config['paths']['vmcoderoot']);
        $vmProjectDir = Utils::processPath($this->config['paths']['vmprojectroot']);
        $createSite = isset($this->config['homestead']['options']['create']['site']) ? $this->config['homestead']['options']['create']['site'] : FALSE;
        $createDb = isset($this->config['homestead']['options']['create']['db']) ? $this->config['homestead']['options']['create']['db'] : FALSE;
        $siteName = isset($this->config['homestead']['site']['hostname']) ? $this->config['homestead']['site']['hostname'] : FALSE;
        $dbDetails = [];

        if($createSite && !$siteName) {
          $output->writeln('<error>Error processing the application config: homestead:site:hostname is required to create a site</error>');
          return DevspotCommandBase::DS_CONFIG_FILE_ERROR;
        }

        if($createDb) {
          if(!$this->dotenv) {
            $output->writeln('<error>Error processing the application config: .env database details are required to create a database</error>');
            return DevspotCommandBase::DS_CONFIG_FILE_ERROR;
          }

          $dbDetails = Utils::getDotEnvDB();
        }

        $output->writeln('Preparing Homestead');

        $homesteadYaml = Yaml::parse(file_get_contents($homesteadYamlPath));

        $table = new Table($output);

        $tableRows = array(
            array('Config', $input->getOption('config')),
            array('Root Directory', $rootCodeDir),
            array('Code Directory', $woCodeDir),
            array('Project Directory', $projectDir),
            array('Homestead Code Directory', $vmCodeRoot),
            array('Homestead Project Directory', $vmProjectDir),
            array('Homestead Config', $homesteadYamlPath),
          );

        $tableRows[] = ['Create Site', $createSite ? 'Yes' : 'No'];
        if($createSite) {
          $tableRows[] = ['Site', $siteName];
        }

        $tableRows[] = ['Create DB', $createDb ? 'Yes' : 'No'];
        if($createDb) {
          foreach($dbDetails as $k => $v) {
            $tableRows[] = ['DB ' . $k, $v];
          }
        }

        $table
          ->setHeaders(array('Key', 'Value'))
          ->setRows($tableRows);

        $table->render();

        $homesteadYaml['folders'][0]['map'] = $woCodeDir;

        $output->writeln('Checking directory: ' . $woCodeDir);
        if(!file_exists($woCodeDir))
        {
          $output->writeln("\t" . 'Creating ' . $woCodeDir);
          mkdir($woCodeDir);
        }

        if(!is_dir($woCodeDir)) {
          $output->writeln('<error>Error creating code directory: ' . $woCodeDir  . '</error>');
          return DevspotCommandBase::DS_CODE_DIR_ERROR;
        }

        if($createSite) {
          $foundSite = FALSE;
          foreach($homesteadYaml['sites'] as $index => $site) {
            if($site['map'] == $siteName) {
              $homesteadYaml['sites'][$index]['to'] = Utils::processPath($vmProjectDir . '/public');
              $output->writeln('Found site: ' . $siteName);
              $foundSite = TRUE;
              break;
            }
          }

          if(!$foundSite) {
              $homesteadYaml['sites'][] = ['map' => $siteName, 'to' => Utils::processPath($projectDir . '/public')];
              $output->writeln('Adding site: ' . $siteName);
          }

        }

        if($createDb) {
          
          $foundDb = FALSE;
          foreach($homesteadYaml['databases'] as $index => $db) {
            if($db == $dbDetails['database']) {
              $foundDb = TRUE;
              $output->writeln('Found db: ' . $db);
              break;
            }
          }

          if(!$foundDb) {
              $homesteadYaml['databases'][] = $dbDetails['database'];
              $output->writeln('Adding db: ' . $db);
          }
        }

        file_put_contents($homesteadYamlPath, Yaml::dump($homesteadYaml));

        $output->writeln('You can now reprovision homestead by running: <info>vagrant reload --provision</info> from your Homestead path (usually ~/Homestead)');
        $output->writeln('You should also check that you can lookup the DNS name for "http://'.$siteName.'"');
        $output->writeln('A simple way to do this is to check that your system\'s hosts contains the line: ');
        $output->writeln("\t\t" . $homesteadYaml['ip'] . "\t" . $siteName);

    }

}
