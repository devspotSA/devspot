<?php namespace WorkshopOrange\Devspot;

use Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class RollDeploymentCommand extends DevspotCommandBase
{
  const DS_APP_STATE_ERROR = 253;
  const DS_APP_MIGRATE_ERROR = 252;
  const DS_APP_DB_ERROR = 251;

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('roll-deployment')
            ->setDescription('Roll a deployment of the current app')
            ->setHelp("This will run a series of commands to deploy the current app. Requires a devspot.yml file.");

        $this->addOption('drop-and-reseed','dar',
          InputOption::VALUE_NONE,
          'Specify that if there are migrations to run, rather drop, migrate, and reseed the database');

        $this->addOption('force-drop-and-reseed','fdar',
          InputOption::VALUE_NONE,
          'Force a run of drop, migrate, and reseed the database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $appEnv = NULL;
        try {
          self::setOutput($output);
          self::prepareConfig($input->getOption('config'));
          
          $appEnv = env('APP_ENV');
          if(!$appEnv) {
            throw new Exception('Could not determine the application environment');
          }
      
          if(!isset($this->config['deploy'][$appEnv])) {
            throw new Exception('There is not deployment information for APP_ENV ' . $appEnv);
          }

          if(isset($this->config['deploy'][$appEnv]['slack-ping-url']) && isset($this->config['deploy'][$appEnv]['slack-channel'])) {
           $this->setRemoteSlackURL($this->config['deploy'][$appEnv]['slack-ping-url'], $this->config['deploy'][$appEnv]['slack-channel']);
          }

        } catch(Exception $ex) {
          $output->writeln('ERROR: Error processing the application config: ' . $ex->getMessage());
          return DevspotCommandBase::DS_CONFIG_FILE_ERROR;
        }

        if($input->getOption('drop-and-reseed')) {
          $this->config['deploy'][$appEnv]['drop-and-reseed'] = $input->getOption('drop-and-reseed');
        }

        if(!isset($this->config['deploy'][$appEnv]['drop-and-reseed'])) {
          $this->config['deploy'][$appEnv]['drop-and-reseed'] = FALSE; 
        }

        $woCodeDir = Utils::processPath($this->config['paths']['code']);
        $rootCodeDir = Utils::processPath($this->config['paths']['root']);
        $projectDir = Utils::processPath($this->config['paths']['project']);

        $this->logln('Preparing to roll a deployment');
        $this->broadcastMessage('Preparing to roll a deployment'); 

        if(! Utils::runLocalArtisanTestExistsDB()) {
          $this->logln('Database test failed... trying to create', 'WARNING');
          $this->broadcastMessage('Database test failed... trying to create', ':fire:');
          Utils::runLocalArtisanResetDB(TRUE);
          if(! Utils::runLocalArtisanTestExistsDB()) {
            $this->logln('Database test failed', 'ERROR');
            $this->broadcastMessage('Database test failed again. Bailing.', ':fire:');
            return self::DS_APP_DB_ERROR;
          } 
          $this->broadcastMessage('Database test passed', ':white_check_mark:');
        }

        if($input->getOption('force-drop-and-reseed')) {
          $this->logln('FORCE: Resetting the Database');
          $this->broadcastMessage('Force resetting the database', ':white_check_mark:');
          Utils::runLocalArtisanResetDB();
        }

        $appMigrationsPending = Utils::getLocalArtisanMigrationPendingCount();
        if($appMigrationsPending == -1) {
          Utils::runLocalArtisanMigrateInstall();
          $this->logln('Migrations installed.');
          $this->broadcastMessage('Migrations installed', ':white_check_mark:');
        }
        $appMigrationsPending = Utils::getLocalArtisanMigrationPendingCount();

        $this->logln('Config' . "\t\t\t: " . $this->config['config']);
        $this->logln('Root Directory' . "\t\t: " . $rootCodeDir);
        $this->logln('Code Directory' . "\t\t: " . $woCodeDir);
        $this->logln('Project Directory' . "\t\t: " . $projectDir);
        $this->logln('App Env' . "\t\t\t: " . $appEnv);
        $this->logln('Migrations Pending' . "\t\t: " . $appMigrationsPending);
        $this->logln('Migrations Strategy' . "\t\t: " . ($this->config['deploy'][$appEnv]['drop-and-reseed'] ? 'Drop and Reseed' : 'Migrate Up'));
      
        if($appMigrationsPending > 0) {
          if($this->config['deploy'][$appEnv]['drop-and-reseed']) {
            $this->logln('There are migrations to process, running drop, migration, and reseed...');
            $this->broadcastMessage('There are migrations to process, running drop, migration, and reseed...');

            $this->logln("\tDropping...");
            Utils::runLocalArtisanResetDB();

            $this->logln("\tMigrate...");
            Utils::runLocalArtisanMigrate();

            $this->logln("\tSeeding...");
            Utils::runLocalArtisanSeedDB();

          } else {
            $this->logln('There are migrations to process, running migration...');
            Utils::runLocalArtisanMigrate();
            $this->logln('Migrations complete.');
          }

          $nowAppMigrationsPending = Utils::getLocalArtisanMigrationPendingCount();
          if($nowAppMigrationsPending > 0) {
            $this->logln('There are still ' . $nowAppMigrationsPending . ' migrations to run after running migrations','ERROR');
            $this->broadcastMessage('There are still ' . $nowAppMigrationsPending . ' migrations to run after running migrations',':fire:');
            return self::DS_APP_MIGRATE_ERROR;
          }

          $this->logln("Database processed");
          $this->broadcastMessage("Database processed",":floppy_disk:");
        } else {
          $this->broadcastMessage('There are no migrations to process.',':white_check_mark:');
        }

        $this->broadcastMessage("Deployment complete.",":green_apple:");
    }

}
