<?php namespace WorkshopOrange\Devspot;

class Utils
{
    public static function expandTilde($path)
    {
        if (function_exists('posix_getuid') && strpos($path, '~') !== false) {
            $info = posix_getpwuid(posix_getuid());
            $path = str_replace('~', $info['dir'], $path);
        }

        return $path;
    }

    public static function removeMultiSlash($path) {
      while( strpos($path,"//") !== FALSE ) {
          $path = preg_replace("|//|","/", $path);
      }

      return $path;
    }

    public static function processPath($path)
    {
      $path = self::expandTilde($path);
      $path = self::removeMultiSlash($path);

      return $path;
    }

    public static function getLocalArtisanAppState() {
      $result = self::runLocalArtisanCommand("env");
      if(preg_match('/:(.*)/', $result, $mat)) {
        return trim(strtolower($mat[1]));
      }

      return 'unknown';
    }

    public static function getLocalArtisanMigrationPendingCount() {
      $results = self::runLocalArtisanCommand("migrate:status");
      if(!is_array($results) && preg_match("/No migrations found./", $results)) { 
        return -1;
      }

      if(!is_array($results)) { 
        return 0;
      }

      $c = 0;
      foreach($results as $result) {
        if(preg_match('/^\| N/', $result, $mat)) {
          $c++;
        }
      }

      return $c;
    }

    public static function runLocalArtisanMigrate() {
      self::runLocalArtisanCommand("migrate --force");
    }

    public static function runLocalArtisanMigrateInstall() {
      self::runLocalArtisanCommand("migrate:install");
    }

    public static function runLocalArtisanCommand($command) {
      $output = [];
      exec("/usr/bin/env php artisan -n " . $command, $output);
      if(count($output) == 1) {
        return $output[0];
      }

      return $output;
    }

    public static function runLocalArtisanResetDB($ignoreDropFail = FALSE) {
      $dbDetails = self::getDotEnvDB();

      $connectFunc = "runLocalArtisanDBConnect" . ucfirst(strtolower($dbDetails['connection']));
      $dropFunc = "runLocalArtisanDBDrop" . ucfirst(strtolower($dbDetails['connection']));
      $createFunc = "runLocalArtisanDBCreate" . ucfirst(strtolower($dbDetails['connection']));

      if(! method_exists(self::class, $connectFunc)) {
        throw new Exception('Implementation Missing: ' . $connectFunc);
      }

      if(! method_exists(self::class, $dropFunc)) {
        throw new Exception('Implementation Missing: ' . $dropFunc);
      }


      if(! method_exists(self::class, $createFunc)) {
        throw new Exception('Implementation Missing: ' . $createFunc);
      }

      $connection = self::$connectFunc($dbDetails);

      try {
        self::$dropFunc($connection, $dbDetails['database']);  
      } catch(Exception $ex) {
        if(!$ignoreDropFail) {
          throw $ex;
        }
      }

      self::$createFunc($connection, $dbDetails['database']);  
    }

    public static function runLocalArtisanTestExistsDB() {
      $dbDetails = self::getDotEnvDB();

      $connectFunc = "runLocalArtisanDBConnect" . ucfirst(strtolower($dbDetails['connection']));
      $testFunc = "runLocalArtisanDBTestExists" . ucfirst(strtolower($dbDetails['connection']));

      if(! method_exists(self::class, $connectFunc)) {
        throw new Exception('Implementation Missing: ' . $connectFunc);
      }

      if(! method_exists(self::class, $testFunc)) {
        throw new Exception('Implementation Missing: ' . $testFunc);
      }

      $connection = self::$connectFunc($dbDetails);
      return self::$testFunc($connection, $dbDetails['database']);  
    }

    public static function runLocalArtisanDBTestExistsMysql($connection, $database) {
      if(!mysqli_select_db($connection, $database)) { 
        return FALSE;
      } 

      return TRUE;
    }

    public static function runLocalArtisanDBConnectMysql($dbDetails) {
      $db = mysqli_connect($dbDetails['host'], $dbDetails['username'], $dbDetails['password']);
      if(!$db) {
        throw new Exception('Error connection to MySQL server.');
      }

      return $db;
    }

    public static function runLocalArtisanDBDropMysql($connection, $database) {
      if(!mysqli_query($connection, "drop database " . $database )) { 
        throw new Exception('Error dropping database.');
      }

      return TRUE;
    }

    public static function runLocalArtisanDBCreateMysql($connection, $database) {
      if(!mysqli_query($connection, "create database " . $database )) { 
        throw new Exception('Error createing database.');
      }

      return TRUE;
    }

    public static function getDotEnvDB() {
      $connection = env('DB_CONNECTION');
      $username = env('DB_USERNAME');
      $password = env('DB_PASSWORD');
      $database = env('DB_DATABASE');
      $port = env('DB_PORT');
      $host = env('DB_HOST');

      return [
        'connection' => $connection,
        'username' => $username,
        'password' => $password,
        'database' => $database,
        'port' => $port,
        'host' => $host
      ];
    }

    public static function runLocalArtisanSeedDB() {
      self::runLocalArtisanCommand("db:seed");
    }
}
