<?php namespace WorkshopOrange\Devspot;

use Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;

abstract class DevspotCommandBase extends Command
{

    const DS_CONFIG_FILE_ERROR = 255;
    const DS_CODE_DIR_ERROR = 254;

    const DS_CONFIG_FILE_DEFAULT = "devspot.yml";
    const DS_DEFAULT_HOMESTEAD_CONFIG = "~/.homestead/Homestead.yaml";
    const DS_DEFAULT_PATH_CODE = "~/WO";
    const DS_DEFAULT_PATH_ROOT = "~/WO/Code";
    const DS_DEFAULT_VM_CODE_ROOT = "/home/vagrant/code";

    protected $config = [];

    protected $dotenv = NULL;
    protected $output = NULL;
    protected $remoteSlackURL = NULL;
    protected $remoteSlackChannel = NULL;

    protected function setRemoteSlackURL($url, $channel) {
      $this->remoteSlackURL = $url;
      $this->remoteSlackChannel = $channel;
    }

    protected function broadcastMessage($line, $indicator = '') {
      if($this->remoteSlackURL) {
        $this->broadcastToSlack($line, $indicator);
      }
    }

    /**
     * A utility function to deploy the laravel installation
     * mostly for use in OpsWorks
     */ 
		protected function broadcastToSlack($line, $indicator = '') {
			$iconEmoji = ':devspot:';
			$HOSTNAME = trim(`hostname`);
			$SLACK_DEPLOY_URL = $this->remoteSlackURL;
			$SLACK_DEPLOY_CHANNEL = $this->remoteSlackChannel;
			$APP_SHORT_NAME = getenv('APP_SHORT_NAME');
			$APP_NAME = strtolower(trim($HOSTNAME . '-' . $APP_SHORT_NAME));
			$SLACK_USERNAME = "DevSpot";

			if($SLACK_DEPLOY_CHANNEL && $SLACK_DEPLOY_URL) {
				if($indicator) { $line = $indicator . " : " . $line; }

				$data = "payload=" . json_encode(array(
					"channel"       => $SLACK_DEPLOY_CHANNEL,
					"text"          => $APP_NAME . ': ' . $line,
					"icon_emoji"    => $iconEmoji,
					"username" 			=> $SLACK_USERNAME
				));

				$ch = curl_init($SLACK_DEPLOY_URL);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$result = curl_exec($ch);
				curl_close($ch);

				return $result;
			}
		}

    protected function setOutput($output) {
      $this->output = $output;
    }

    protected function logln($line, $type = "INFO", $time = NULL) {
      if(!$time || !is_numeric($time)) {
        $time = time();
      }

      $typeStr = '';
      if($type) {
        $typeStr = $type  . ' ' ;
      }
      
      $timeStr = date('Y-m-d H:i:s', $time);
      $this->output->writeln($timeStr . '] ' . $typeStr . $line);
    }

    protected function configure()
    {
        $this->addOption('config','c',
          InputOption::VALUE_REQUIRED,
          'Specify a config file path',
          getcwd() . "/" . self::DS_CONFIG_FILE_DEFAULT);
    }

    protected function prepareConfig($configFilePath = NULL, $requireConfigFile = TRUE)
    {
        if(is_null($configFilePath)) {
          $configFilePath = self::DS_CONFIG_FILE_DEFAULT;
        }

        $configFilePath = Utils::processPath($configFilePath);

        if($requireConfigFile && !file_exists($configFilePath)) {
          throw new Exception($configFilePath . " not found");
        }

        if(file_exists($configFilePath)) {
          $this->config = Yaml::parse(file_get_contents($configFilePath));
        } else {
          $this->config = [];
        }

        $this->config['config'] = $configFilePath;

        // The command will always be run from the project director
        $this->config['paths']['project'] = getcwd();

        // Load up the environment
        if(file_exists(getcwd() . "/.env")) {
          $this->dotenv = new Dotenv\Dotenv(getcwd());
          $this->dotenv->load();
        }

        if(empty($this->config['HomesteadConfig'])) {
          $this->config['HomesteadConfig'] = self::DS_DEFAULT_HOMESTEAD_CONFIG;
        }

        if(!is_array($this->config['paths'])) {
          $this->config['paths'] = [];
        }

        if(empty($this->config['paths']['root'])) {
          $this->config['paths']['root'] = self::DS_DEFAULT_PATH_ROOT;
        }

        if(empty($this->config['paths']['code'])) {
          $this->config['paths']['code'] = self::DS_DEFAULT_PATH_CODE;
        }

        if(empty($this->config['paths']['vmcoderoot'])) {
          $this->config['paths']['vmcoderoot'] = self::DS_DEFAULT_VM_CODE_ROOT;
        }

        if(empty($this->config['paths']['vmprojectroot'])) {
          $path_parts = pathinfo($this->config['paths']['project']);
          $this->config['paths']['vmprojectroot'] = $this->config['paths']['vmcoderoot'] . '/' . $path_parts['basename'];
        }

        $this->config['HomesteadConfig'] = Utils::processPath($this->config['HomesteadConfig']);
        $this->config['paths']['root'] = Utils::processPath($this->config['paths']['root']);
        $this->config['paths']['code'] = Utils::processPath($this->config['paths']['code']);
    }

    protected function writeBroadcastLn($output, $line, $localOnly = FALSE, $time = NULL) {
      if(!$time || !is_numeric($time)) {
        $time = time();
      }

      $timeStr = date('Y-m-d H:i:s', $time);

      $output->writeln($timeStr . '] ' . $line);
    }
}
