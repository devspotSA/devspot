<?php namespace WorkshopOrange\Devspot;

use Dotenv;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class RemoteSshCommand extends DevspotCommandBase
{
    const SERVER_NOT_FOUND = 240;

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('ssh')
            ->setDescription('Access Remote Servers')
            ->setHelp("Gain access to remote servers");

        $this->addArgument('server', InputArgument::OPTIONAL, 'Which server do you want to access?');

        $this->addOption('ssh-user','',
          InputOption::VALUE_OPTIONAL,
          'Specify that if there are migrations to run, rather drop, migrate, and reseed the database');
          
        $this->addOption('list','',
          InputOption::VALUE_NONE,
          'List the available servers');
          
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $server = $input->getArgument('server');
        if(!$server) {
          $server = 'local';
        }

        $sshUser = $input->getOption('ssh-user');

        try {
          self::prepareConfig($input->getOption('config'), FALSE);

          if(!isset($this->config['servers'])) {
            throw new Exception('The following config keys are required: servers, servers:<server>');
          }
        } catch(Exception $ex) {
          $output->writeln('<error>Error processing the application config: ' . $ex->getMessage() . '</error>');
          return DevspotCommandBase::DS_CONFIG_FILE_ERROR;
        }

        $homesteadYamlPath =  Utils::processPath($this->config['HomesteadConfig']);
        $homesteadYaml = Yaml::parse(file_get_contents($homesteadYamlPath));
        if(!isset($this->config['servers']['local'])) {
          $this->config['servers']['local'] = [
            'host' => $homesteadYaml['ip'], 
            'approot' => $this->config['paths']['vmprojectroot'],
            'user' => 'vagrant'
          ];
        }

        if(!isset($this->config['servers'][$server])) {
          $output->writeln('<error>Server not found: ' . $server.'</error>');
          return self::SERVER_NOT_FOUND;
        }

        if($input->getOption('list')) {
          $table = new Table($output);
          $tableRows = [];
          
          foreach($this->config['servers'] as $serverName => $server) {
            if(!$server['user']) { $server['user'] = 'forge'; }

            $remote = $server['user'] . '@' . $server['host'];
            $tableRows[] = [$serverName, $remote, $server['approot']];
          }

          $table
            ->setHeaders(array('Server', 'Remote', 'Root'))
            ->setRows($tableRows);

          $table->render();
          return;
        }

        if(!$sshUser && $server == 'local') {
          $sshUser = 'vagrant';
        }

        if(!$sshUser && $server != 'local') {
          if($this->config['servers'][$server]['user']) {
            $sshUser = $this->config['servers'][$server]['user'];
          } else {
            $sshUser = 'forge';
          }
        }

        $remote = $sshUser . '@' . $this->config['servers'][$server]['host'];
        if(isset($this->config['servers'][$server]['approot'])) {
          passthru('/usr/bin/env ssh -t ' . $remote .' "cd ' . $this->config['servers'][$server]['approot'] . ' ; bash"');
        } else {
          passthru('/usr/bin/env ssh -t ' . $remote);
        }
    }

}
